const connect = require("./connect");

module.exports = function testConnect(){
    try{
    const query = `SELECT 'Conexão bem-sucedida' AS mensagem`;
    connect.query(query, function(err){
        if(err){
            console.log("erro na conexão:" + err) 
            return;
        }
        console.log("conexão realizada com Mysql!");
    })
}catch(error){
    console.error("erro ao executar a consulta:", error);
}
};