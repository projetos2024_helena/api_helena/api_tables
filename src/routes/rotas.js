const router = require("express").Router();
const dbController = require("../controller/dbController");

//rota para consulta das tabelas do banco
router.get("/tables/getnometables", dbController.getnometables);
router.get("/tables/getdescricaotables", dbController.getdescricaotables)

module.exports = router;
